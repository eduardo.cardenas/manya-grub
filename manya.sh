#!/bin/bash

if ! [ $(id -u) = 0 ]; then
   echo "necesito root." >&2
   exit 1
fi

if [ $SUDO_USER ]; then
real_user=$SUDO_USER
lsblk
read -p "escoja numero sda /boot/efi: " bootefi
read -p "escoja numero sda raiz(/): " raiz

sudo su << EOF
mount /dev/sda$raiz /mnt
mount /dev/sda$bootefi /mnt/boot/efi
lsblk
cd /mnt
mount -t proc proc /mnt/proc
mount -t sysfs sys /mnt/sys
mount -o bind /dev /mnt/dev
mount -t devpts pts /mnt/dev/pts
sudo pacman -Syy grub --no-confirm
chroot /mnt
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=manjaro recheck
exit
mpdprobe efivarfs
chroot /mnt
mount -t efivarfs efivarfs /sys/firmware/efi/efivars
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=manjaro recheck
update-grub
exit
exit
echo LISTO :D
EOF

else
	real_user=$(whoami)
fi

